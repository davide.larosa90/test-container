FROM alpine:latest

RUN apk update && \
		apk upgrade && \
		apk add curl vim tmux wget bash util-linux jq git python3 docker openrc

#TODO ADD		jd

ENTRYPOINT ["tail", "-f", "/dev/null"]
