# Test container
This is a dockerfile that will help on applications troubleshooting

The base image is alpine:latest and there are some usefull installations like

- vim
- curl
- jq
- others

This container, on kubernetes, will log nothing. It's entrypoint is *tail -f /dev/null*.

## Build
Use this container as custom container from where you can query other containers in you docker/kubernetes environment

##### 1. Clone this repo:

``` code
git clone https://gitlab.com/davide.larosa90/test-container.git
cd test-container
```
##### 2. Build your own container

``` code
docker build -t <name>:<tag> .
```

##### 3. Run it on docker

``` code
docker run -it <name>:<tag>
```

## Prebuilt image
You can download a prebuilt image from dockerhub at davidelarosa/test-container:latest

```code
docker pull davidelarosa/test-container:latest
```
Fell free to improve it!
